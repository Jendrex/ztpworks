The fog turned out to be much more dense than expected - you can
barely see 1 meter ahead of you.
When you walk forward a bit you can see a great crater with something
shiny at the bottom to your right. The curiosity is making you anxious
to check what is it yet your logic tells you it's better to ignore it.