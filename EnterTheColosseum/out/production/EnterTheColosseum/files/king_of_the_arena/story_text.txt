    You have beaten the King of the Arena. Now you become one
of them. Your glory and power are great, but you will never be
able to see your family again. Congratulations, you have achieved
the good ending.