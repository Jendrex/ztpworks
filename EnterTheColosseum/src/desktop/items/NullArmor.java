package desktop.items;

import desktop.main.Game;

/**
 * Created by sparrow on 10.12.16.
 */
public class NullArmor extends Armor {

    public NullArmor() {
        name = "No armor";
        description = "";
    }

    @Override
    public void addBonus() {}

    @Override
    public void removeBonus() {}

    @Override
    public boolean isNull() {
        return true;
    }
}
