package desktop.items;

/**
 * Created by sparrow on 10.12.16.
 */
public abstract class Weapon extends Item {
    protected int perceptionMod;
    protected int strengthMod;
    protected int magicMod;
    protected int rangedDamageMod;
    protected int meleeDamageMod;
    protected int magicDamageMod;

    public int getPerceptionMod() {
        return perceptionMod;
    }

    public void setPerceptionMod(int perceptionMod) {
        this.perceptionMod = perceptionMod;
    }

    public int getStrengthMod() {
        return strengthMod;
    }

    public void setStrengthMod(int strengthMod) {
        this.strengthMod = strengthMod;
    }

    public int getMagicMod() {
        return magicMod;
    }

    public void setMagicMod(int magicMod) {
        this.magicMod = magicMod;
    }

    public int getRangedDamageMod() {
        return rangedDamageMod;
    }

    public void setRangedDamageMod(int rangedDamageMod) {
        this.rangedDamageMod = rangedDamageMod;
    }

    public int getMeleeDamageMod() {
        return meleeDamageMod;
    }

    public void setMeleeDamageMod(int meleeDamageMod) {
        this.meleeDamageMod = meleeDamageMod;
    }

    public int getMagicDamageMod() {
        return magicDamageMod;
    }

    public void setMagicDamageMod(int magicDamageMod) {
        this.magicDamageMod = magicDamageMod;
    }


}
