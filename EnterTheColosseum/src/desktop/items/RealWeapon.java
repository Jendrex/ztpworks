package desktop.items;

import desktop.main.Game;

/**
 * Created by Sparrow on 18.10.16.
 */
public class RealWeapon extends Weapon {

    private RealWeapon(final Builder builder){
        this.perceptionMod = builder.perceptionMod;
        this.strengthMod = builder.strengthMod;
        this.magicMod = builder.magicMod;
        this.rangedDamageMod = builder.rangedDamageMod;
        this.meleeDamageMod = builder.meleeDamageMod;
        this.magicDamageMod = builder.magicDamageMod;
        this.cost = builder.cost;
        this.name = builder.name;
        this.description = builder.description;
    }

    @Override
    public void addBonus() {
        Game.character.setPerception(Game.character.getPerception() + perceptionMod);
        Game.character.setStrength(Game.character.getStrength() + strengthMod);
        Game.character.setMagic(Game.character.getMagic() + magicMod);
        Game.character.setRangedDamage(Game.character.getRangedDamage() + rangedDamageMod);
        Game.character.setMeleeDamage(Game.character.getMeleeDamage() + meleeDamageMod);
        Game.character.setMagicDamage(Game.character.getMagicDamage() + magicDamageMod);
    }

    @Override
    public void removeBonus() {
        Game.character.setPerception(Game.character.getPerception() - perceptionMod);
        Game.character.setStrength(Game.character.getStrength() - strengthMod);
        Game.character.setMagic(Game.character.getMagic() - magicMod);
        Game.character.setRangedDamage(Game.character.getRangedDamage() - rangedDamageMod);
        Game.character.setMeleeDamage(Game.character.getMeleeDamage() - meleeDamageMod);
        Game.character.setMagicDamage(Game.character.getMagicDamage() - magicDamageMod);
    }

    @Override
    public boolean isNull() {
        return false;
    }

    public static class Builder{
        private int perceptionMod;
        private int strengthMod;
        private int magicMod;
        private int rangedDamageMod;
        private int meleeDamageMod;
        private int magicDamageMod;
        private int cost;
        private String name;
        private String description;

        public Builder perceptionMod(final int perceptionMod){
            this.perceptionMod = perceptionMod;
            return this;
        }

        public Builder strengthMod(final int strengthMod){
            this.strengthMod = strengthMod;
            return this;
        }

        public Builder magicMod(final int magicMod){
            this.magicMod = magicMod;
            return this;
        }

        public Builder rangedDamageMod(final int rangedDamageMod){
            this.rangedDamageMod = rangedDamageMod;
            return this;
        }

        public Builder meleeDamageMod(final int meleeDamageMod){
            this.meleeDamageMod = meleeDamageMod;
            return this;
        }

        public Builder magicDamageMod(final int magicDamageMod){
            this.magicDamageMod = magicDamageMod;
            return this;
        }

        public Builder cost(final int cost){
            this.cost = cost;
            return this;
        }

        public Builder name(final String name){
            this.name = name;
            return this;
        }

        public Builder description(final String description){
            this.description = description;
            return this;
        }

        public RealWeapon build(){
            return new RealWeapon(this);
        }
    }
}
