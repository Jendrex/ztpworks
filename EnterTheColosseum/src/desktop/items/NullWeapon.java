package desktop.items;

import desktop.main.Game;

/**
 * Created by sparrow on 10.12.16.
 */
public class NullWeapon extends Weapon {

    public NullWeapon(){
        this.name = "No weapon";
        this.description = "";
    }

    @Override
    public void addBonus() {}

    @Override
    public void removeBonus() {}

    @Override
    public boolean isNull() {
        return true;
    }
}
