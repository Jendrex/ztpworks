package desktop.items;


/**
 * Created by Sparrow on 18.10.16.
 */
public abstract class Item {
    protected int cost;             //koszt
    protected String name;          //nazwa
    protected String description;   //opis

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public abstract void addBonus();
    public abstract void removeBonus();
    public abstract boolean isNull();
}
