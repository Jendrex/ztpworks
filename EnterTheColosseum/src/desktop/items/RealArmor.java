package desktop.items;


import desktop.main.Game;

/**
 * Created by Sparrow on 18.10.16.
 */

public class RealArmor extends Armor {

    private RealArmor(final Builder builder){
        this.hpMod = builder.hpMod;
        this.cost = builder.cost;
        this.name = builder.name;
        this.description = builder.description;
    }

    @Override
    public void addBonus() {
        Game.character.setHealth(Game.character.getHealth() + hpMod);
        Game.character.setCurrentHealth(Game.character.getCurrentHealth() + hpMod);
    }

    @Override
    public void removeBonus() {
        int healthAfterRemoval;
        Game.character.setHealth(healthAfterRemoval = (Game.character.getHealth() - hpMod));
        if(Game.character.getCurrentHealth() > healthAfterRemoval){
            Game.character.setCurrentHealth(healthAfterRemoval);
        }
    }

    @Override
    public boolean isNull() {
        return false;
    }

    public static class Builder{
        private int hpMod;
        private int cost;
        private String name;
        private String description;

        public Builder hpMod(final int hpMod){
            this.hpMod = hpMod;
            return this;
        }

        public Builder cost(final int cost){
            this.cost = cost;
            return this;
        }

        public Builder name(final String name){
            this.name = name;
            return this;
        }

        public Builder description(final String description){
            this.description = description;
            return this;
        }

        public RealArmor build(){
            return new RealArmor(this);
        }
    }
}