package desktop.items;

/**
 * Created by sparrow on 10.12.16.
 */
public abstract class Armor extends Item {
    protected int hpMod;

    public int getHpMod() {
        return hpMod;
    }

    public void setHpMod(int hpMod) {
        this.hpMod = hpMod;
    }
}
