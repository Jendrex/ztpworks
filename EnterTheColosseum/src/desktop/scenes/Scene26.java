package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Scene26 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene26/story_text.txt";

    public Scene26(){
        super();
        setStrings();
    }

    @Override
    protected void putStringsSequentially() {
        int y = 0;
        for (String s: storyText) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+4, y+5, s.charAt(i) + "", Terminal.Color.WHITE, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(3);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }

        y = 0;
        for (String s: optionStrings) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+3, y+35, s.charAt(i) + "", Terminal.Color.CYAN, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(8);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any key to continue you lucky bastard");
        title = "HERE WE GO.. <UNZIP>";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            //place your debuffs here
            MagicalSoap = false;
            GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
            return new Scene1();
        }
        return null;
    }
}
