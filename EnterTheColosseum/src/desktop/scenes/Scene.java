package desktop.scenes;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.EmptySpace;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.Table;
import com.googlecode.lanterna.gui.layout.VerticalLayout;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalSize;
import desktop.tools.AlterString;
import desktop.tools.IAlterString;
import desktop.tools.SequenceString;
import desktop.windows.MainWindow;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Sparrow on 18.10.16.
 */
public abstract class Scene {
    protected String title;
    protected ArrayList<String> storyText;
    protected ArrayList<String> optionStrings;
    protected ArrayList<Action> actions;
    public static Screen screen;
    public static final String WRONG_OPTION = "Please select correct number.";
    public static boolean MagicalSoap = false;
    public static boolean moleVisited = false;
    private IAlterString wrong = new SequenceString(new AlterString(WRONG_OPTION));


    public Scene(){
        MainWindow mainWindow = MainWindow.getMainWindow();
        screen = mainWindow.getScreen();
        storyText = new ArrayList<String>();
        optionStrings = new ArrayList<String>();
        actions = new ArrayList<Action>();
    }

    public static void reset(){
        Scene2.visited = false;
        MagicalSoap = false;
        moleVisited = false;
    }

    public String readLine(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int c;
        for (c = inputStream.read(); c != '\n' && c != -1 ; c = inputStream.read()) {
            byteArrayOutputStream.write(c);
        }
        if (c == -1 && byteArrayOutputStream.size() == 0) {
            return null;
        }
        String line = byteArrayOutputStream.toString("UTF-8");
        return line;
    }

    protected void putTitle(){
        screen.putString(3, 3, title, Terminal.Color.YELLOW, Terminal.Color.BLACK);
    }

    protected void putStringsSequentially(){
        int y = 0;
        for (String s: storyText) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+4, y+5, s.charAt(i) + "", Terminal.Color.WHITE, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(3);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }

        y = 0;
        for (String s: optionStrings) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+3, y+20, s.charAt(i) + "", Terminal.Color.CYAN, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(8);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }
    }

    public final Scene doAll() {
        MainWindow.getMainWindow().getScreen().clear();
        putTitle();
        putStringsSequentially();
        Key key;

        do {
            key = screen.readInput();
        }while(stayHere(key));

        return nextScene(key);
    }

    protected abstract Scene nextScene(Key key);
    protected Boolean stayHere(Key key)
    {
        if(key!=null)
        {
            if(Character.getNumericValue(key.getCharacter())<=optionStrings.size())
                return false;
            else
                wrong.print(3,30);
        }
        return true;
    }

    protected abstract void setStrings();
}
