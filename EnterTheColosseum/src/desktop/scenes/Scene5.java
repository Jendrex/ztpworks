package desktop.scenes;

/**
 * Created by sparrow on 10.11.16.
 */

import com.googlecode.lanterna.input.Key;

import java.io.*;

/**
 * Created by Sparrow on 20.10.16.
 */
public class Scene5 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene5/story_text.txt";

    public Scene5(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any button to continue.");
        title = "THE DUNGEON";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            return new Scene1();
        }
        return null;
    }

}
