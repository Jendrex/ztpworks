package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Scene24 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene24/story_text.txt";

    public Scene24(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any key to continue");
        title = "OH OK..";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            MagicalSoap=false;
            GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
            return new Scene1();
        }
        return null;
    }
}
