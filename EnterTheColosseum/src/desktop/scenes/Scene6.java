package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.Game;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene6 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene6/story_text.txt";

    public Scene6(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Go and fight in Colosseum.");
        optionStrings.add("2. Explore the dungeon.");
        optionStrings.add("3. Go to the dungeon exit.");
        optionStrings.add("4. Go back to your room.");
        optionStrings.add("5. Go to the shop.");
        title = "HALLWAY";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                GameMusic.getGameMusic().changeMusic(GameMusic.combatTheme);
                return new CombatScene();

            case '2':
                return new Scene8();

            case '3':
                return new Scene7();

            case '4':
                return new Scene1();

            case '5':
                return new Shop();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
