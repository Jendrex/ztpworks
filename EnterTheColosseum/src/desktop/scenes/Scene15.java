package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene15 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene15/story_text.txt";

    public Scene15(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Go to the Witch's Tower");
        optionStrings.add("2. Return back to the hallway");
        title = "THE VISION";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                return new Scene17();

            case '2':
                return new Scene6();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
