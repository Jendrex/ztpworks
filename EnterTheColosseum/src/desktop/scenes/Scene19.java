package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene19 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene19/story_text.txt";

    public Scene19(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. No wait I'm sorry.. It's just... I haven't seen such a beauty in my entire life");
        optionStrings.add("2. <grabs her> Don't worry baby, with me on your side words won't be able to hurt you, just my **** <kisses her>");
        optionStrings.add("3. <throw a soap in her head and run away> I ain't dealing with no bitches, hoe!");
        optionStrings.add("4. <remain silent>");
        title = "TENSION GETS HIGH";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                return new Scene18();

            case '2':
                GameMusic.getGameMusic().changeMusic(GameMusic.deathTheme);
                return new Scene21();

            case '3':
                return new Scene22();

            case '4':
                return new Scene24();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
