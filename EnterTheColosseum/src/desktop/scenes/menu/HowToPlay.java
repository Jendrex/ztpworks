package desktop.scenes.menu;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.scenes.Scene;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by sparrow on 05.11.16.
 */
public class HowToPlay extends Scene {
    private static final String STORY_TEXT_PATH = "resources/files/how_to_play/how_to_play.txt";
    private int width;

    public HowToPlay(){
        super();
        setStrings();
        width = screen.getTerminalSize().getColumns()/2-storyText.get(0).length()/2;
    }

    @Override
    protected void putTitle() {
        screen.putString(screen.getTerminalSize().getColumns()/2-title.length()/2, 3, title, Terminal.Color.YELLOW, Terminal.Color.BLACK);
    }

    @Override
    protected void putStringsSequentially() {
        int y = 0;
        for (String s: storyText) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+width, y+5, s.charAt(i) + "", Terminal.Color.WHITE, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(3);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }

        y = 0;
        for (String s: optionStrings) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+width, y+35, s.charAt(i) + "", Terminal.Color.CYAN, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(8);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any key to return to the main menu.");
        title = "How to play";

        try (BufferedReader br = new BufferedReader(new FileReader(STORY_TEXT_PATH))) {
            String line;
            while ((line = br.readLine()) != null) {
                storyText.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        while (screen.readInput() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return new Menu();
    }

}
