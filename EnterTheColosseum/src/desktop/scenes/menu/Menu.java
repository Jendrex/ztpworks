package desktop.scenes.menu;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.characters.heroes.Champion;
import desktop.items.NullArmor;
import desktop.items.NullWeapon;
import desktop.main.Game;
import desktop.scenes.Scene;
import desktop.scenes.Scene1;
import desktop.scenes.Scene2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by sparrow on 05.11.16.
 */
public class Menu extends Scene {
    private int width;

    public Menu() {
        super();
        setStrings();
        width = screen.getTerminalSize().getColumns()/2-optionStrings.get(0).length();
    }

    @Override
    protected void putTitle() {
        screen.putString(width-3, 3, title, Terminal.Color.GREEN, Terminal.Color.BLACK);
    }

    @Override
    protected void putStringsSequentially() {
        int y = 0;
        y = 0;
        for (String s: optionStrings) {
            screen.putString(width, y+10, s, Terminal.Color.CYAN, Terminal.Color.BLACK);
            ++y;
        }

        screen.refresh();
    }

    @Override
    protected void setStrings() {
        optionStrings.add("1. NEW GAME");
        optionStrings.add("");
        optionStrings.add("");
        optionStrings.add("2. HOW TO PLAY");
        optionStrings.add("");
        optionStrings.add("");
        optionStrings.add("3. CREDITS");
        optionStrings.add("");
        optionStrings.add("");
        optionStrings.add("4. EXIT");
        title = "ENTER THE COLOSSEUM";

    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                dispose();
                return new Scene1();

            case '2':
                return new HowToPlay();

            case '3':
                return new Credits();

            case '4':
                System.exit(0);
                break;

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }


    private void dispose() {
        Game.character = new Champion("Jack");
        ((Champion)Game.character).setEquippedArmor(new NullArmor());
        ((Champion)Game.character).setEquippedWeapon(new NullWeapon());
        Scene.reset();
    }
}
