package desktop.scenes.menu;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.scenes.Scene;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by sparrow on 05.11.16.
 */
public class Credits extends Scene {
    private int width;

    public Credits(){
        super();
        setStrings();
        width = screen.getTerminalSize().getColumns()/2-storyText.get(0).length()/2;
    }

    @Override
    protected void putTitle() {
        screen.putString(screen.getTerminalSize().getColumns()/2-title.length()/2, 3, title, Terminal.Color.YELLOW, Terminal.Color.BLACK);
    }

    @Override
    protected void putStringsSequentially() {
        int y = 0;
        for (String s: storyText) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+width, y+10, s.charAt(i) + "", Terminal.Color.WHITE, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(15);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }

        y = 0;
        for (String s: optionStrings) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+width-storyText.get(0).length(), y+35, s.charAt(i) + "", Terminal.Color.CYAN, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(15);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any key to return to the main menu.");
        title = "CREDITS";
        storyText.add("--<<Developers>>--");
        storyText.add("Fabian Olszewski");
        storyText.add("Jędrzej Sawicki");
        storyText.add("Jan Romanowski");
        storyText.add("");
        storyText.add("--<<Story>>--");
        storyText.add("Fabian Olszewski");
        storyText.add("Jędrzej Sawicki");
        storyText.add("");
        storyText.add("--<<Concept founder>>--");
        storyText.add("Fabian Olszewski");
        storyText.add("");
        storyText.add("--<<Design patterns>>--");
        storyText.add("Fabian Olszewski");
        storyText.add("Jędrzej Sawicki");
        storyText.add("Jan Romanowski");
    }

    @Override
    protected Scene nextScene(Key key)
    {
        while (screen.readInput() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return new Menu();
    }

}
