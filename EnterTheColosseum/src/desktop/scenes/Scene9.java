package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene9 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene9/story_text.txt";

    public Scene9(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Ignore it and go onward");
        optionStrings.add("2. Make the jump happen #YOLO");
        optionStrings.add("3. Go back");
        title = "THE FOG";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                GameMusic.getGameMusic().changeMusic(GameMusic.witchTheme);
                return new Scene17();

            case '2':
                GameMusic.getGameMusic().changeMusic(GameMusic.deathTheme);
                return new Scene13();

            case '3':
                return new Scene8();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
