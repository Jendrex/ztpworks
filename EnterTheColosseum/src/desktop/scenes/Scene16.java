package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.Game;
import desktop.main.GameMusic;
import desktop.scenes.menu.Menu;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene16 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene16/story_text.txt";
    private final String STORY_TEXT_PATH_DEATH = "files/scene16/story_text_death.txt";
    private final String STORY_TEXT_PATH_NOSOAP = "files/scene16/story_text_nosoap.txt";

    public Scene16() {
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        if(MagicalSoap)
        {
            if (Game.character.getStrength() >= 5) {
                optionStrings.add("1. Throw the soap into the pond");
                optionStrings.add("2. Ignore the pond and move onward");
                title = "Impetuous Pond";

                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
                try {
                    String line;
                    while ((line = readLine(is)) != null){
                        storyText.add(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                optionStrings.add("Press any key to continue");
                title = "Deadly chasm";

                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH_DEATH);
                try {
                    String line;
                    while ((line = readLine(is)) != null){
                        storyText.add(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            if (Game.character.getStrength() >= 5) {
                optionStrings.add("1. Take a quick bath before leaving");
                optionStrings.add("2. Ignore the pond and move onward");
                title = "Impetuous Pond";

                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH_NOSOAP);
                try {
                    String line;
                    while ((line = readLine(is)) != null){
                        storyText.add(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                optionStrings.add("Press any key to continue");
                title = "Deadly chasm";

                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH_DEATH);
                try {
                    String line;
                    while ((line = readLine(is)) != null){
                        storyText.add(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(MagicalSoap)
        {
            if (Game.character.getStrength() >= 5)
            {
                switch (key.getCharacter()) {
                    case '1':
                        MagicalSoap = false;
                        return new Scene15();

                    case '2':
                        GameMusic.getGameMusic().changeMusic(GameMusic.witchTheme);
                        return new Scene17();

                    default:
                        screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                        screen.refresh();
                }
            }
            else
            {
                if (key != null) {
                    reset();
                    GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
                    return new Menu();
                }
            }
        }
        else
        {
            if (Game.character.getStrength() >= 5)
            {
                switch (key.getCharacter()) {
                    case '1':
                        Game.character.setCurrentHealth(Game.character.getHealth());
                        GameMusic.getGameMusic().changeMusic(GameMusic.witchTheme);
                        return new Scene17();

                    case '2':
                        GameMusic.getGameMusic().changeMusic(GameMusic.witchTheme);
                        return new Scene17();

                    default:
                        screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                        screen.refresh();
                }
            }
            else
            {
                if (key != null) {
                    reset();
                    GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
                    return new Menu();
                }
            }
        }
        return null;
    }
}
