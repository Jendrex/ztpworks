package desktop.scenes;

import com.googlecode.lanterna.input.Key;

import java.io.*;

/**
 * Created by sparrow on 12.11.16.
 */
public class KingOfTheArena extends Scene {
    private final String STORY_TEXT_PATH = "files/king_of_the_arena/story_text.txt";

    public KingOfTheArena(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any button to exit game.");
        title = "KING OF THE ARENA";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            System.exit(0);
        }
        return null;
    }
}
