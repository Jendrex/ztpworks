package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.characters.heroes.Champion;
import desktop.main.Game;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene7 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene7/story_text.txt";
    private final String STORY_TEXT_PATH_RICH = "files/scene7/story_text_rich.txt";

    public Scene7() {
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        title = "DUNGEON EXIT";

        if (((Champion) Game.character).getMoney() <= 30) {
            optionStrings.add(" Press any key to go back.");
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
            try {
                String line;
                while ((line = readLine(is)) != null){
                    storyText.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            optionStrings.add("1. Quit the dungeon.");
            optionStrings.add("2. Go back to the hallway.");
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH_RICH);
            try {
                String line;
                while ((line = readLine(is)) != null){
                    storyText.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    protected Scene nextScene(Key key)
    {
        if (((Champion) Game.character).getMoney() > 30) {
            switch (key.getCharacter()) {
                case '1':
                    return new AlternativeEnding();

                case '2':
                    return new Scene6();

                default:
                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                    screen.refresh();
            }
        }
        return new Scene6();
    }
}
