package desktop.scenes;

/**
 * Created by sparrow on 10.11.16.
 */

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.characters.heroes.Champion;
import desktop.items.NullArmor;
import desktop.items.NullWeapon;
import desktop.main.Game;
import desktop.main.GameMusic;
import desktop.scenes.menu.Menu;

import java.io.*;

/**
 * Created by Sparrow on 20.10.16.
 */
public class Die extends Scene {
    private final String STORY_TEXT_PATH = "files/die/story_text.txt";

    public Die(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Go to the main menu.");
        optionStrings.add("2. Exit game.");
        title = "DEAD, DEAD, DEAD, YOUR ARE DEAD, DEARIE";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
                return new Menu();

            case '2':
                System.exit(0);

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }

}
