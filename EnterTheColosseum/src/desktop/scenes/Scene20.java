package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene20 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene20/story_text.txt";

    public Scene20(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. <remain silent>");
        optionStrings.add("2. I can't take it anymore... <grab and kiss her>");
        optionStrings.add("3. What is with all those questions?! You want your soap or not?");
        title = "MORE QUESTIONS...";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                return new Scene25();

            case '2':
                return new Scene23();

            case '3':
                return new Scene24();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
