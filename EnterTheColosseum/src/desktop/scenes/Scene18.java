package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene18 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene18/story_text.txt";

    public Scene18(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any key to continue");
        title = "...AAAND THERE SHE GOES";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            MagicalSoap = false;
            GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
            return new Scene1();
        }
        return null;
    }
}
