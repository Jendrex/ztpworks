package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import com.sun.org.apache.regexp.internal.REDebugCompiler;
import desktop.characters.enemies.ERank;
import desktop.characters.enemies.Enemy;
import desktop.characters.heroes.Champion;
import desktop.main.Game;
import desktop.main.GameMusic;
import desktop.tools.*;
import javafx.scene.paint.Color;

import java.io.*;
import java.util.Random;

/**
 * Created by root on 23.10.16.
 */
public class CombatScene extends Scene {
    private final String STORY_TEXT_PATH = "files/combat_scene/story_text.txt";

    private Enemy enemy;

    //dobrać potwora na podstawie rank(jest), wyśiwetlic opis potwora(nie ma), zrobic walke(nie ma), umieranie(jest), exp up(jest), wygraną jak sie wygra z najmocniejszym rank(jest)

    //putStringsSequentially z wywolaniem czarnym bez sleep'ow
    private void hideStrings(){
        int y = 0;
        for (String s: storyText) {
                  screen.putString(4, y+5, s, Terminal.Color.BLACK, Terminal.Color.BLACK);
        ++y;
    }

    y = 0;
        for (String s: optionStrings) {
        screen.putString(3, y+20, s, Terminal.Color.BLACK, Terminal.Color.BLACK);
        ++y;
    }

        screen.refresh();
    }

    private void showHP(){
        screen.putString(3,13,"HP: "
                        +String.valueOf(Game.character.getCurrentHealth())
                        +"/"
                        +String.valueOf(Game.character.getHealth()),
                Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(30,13,"HP: "
                        +String.valueOf(enemy.getCurrentHealth())
                        +"/"
                        +String.valueOf(enemy.getHealth()),
                Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(3, 30, WRONG_OPTION, Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void showStats(){
        screen.putString(3,12,Game.character.getName(), Terminal.Color.BLUE, Terminal.Color.BLACK);
        screen.putString(30,12,enemy.getName(), Terminal.Color.RED, Terminal.Color.BLACK);
        screen.putString(3,14,"Perception: "+String.valueOf(Game.character.getPerception()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(30,14,"Perception: "+String.valueOf(enemy.getPerception()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(3,15,"Ranged Damage: "+String.valueOf(Game.character.getRangedDamage()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(30,15,"Ranged Damage: "+String.valueOf(enemy.getRangedDamage()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(3,16,"Strength: "+String.valueOf(Game.character.getStrength()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(30,16,"Strength: "+String.valueOf(enemy.getStrength()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(3,17,"Melee Damage: "+String.valueOf(Game.character.getMeleeDamage()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(30,17,"Melee Damage: "+String.valueOf(enemy.getMeleeDamage()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(3,18,"Magic: "+String.valueOf(Game.character.getMagic()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(30,18,"Magic: "+String.valueOf(enemy.getMagic()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(3,19,"Magic Damage: "+String.valueOf(Game.character.getMagicDamage()),Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(30,19,"Magic Damage: "+String.valueOf(enemy.getMagicDamage()),Terminal.Color.WHITE, Terminal.Color.BLACK);
    }

//    private void putCombatString(int range, int height, String text, Terminal.Color color){
//        for (int i = 0; text.length() > i; ++i) {
//            screen.putString(range+i, height, text.charAt(i) + "", color, Terminal.Color.BLACK);
//            screen.refresh();
//            try {
//                Thread.sleep(20);
//            } catch (InterruptedException ex) {
//                Thread.currentThread().interrupt();
//            }
//        }
//    }

    public CombatScene() {
        super();
        setStrings();
    }

    @Override
    protected void putStringsSequentially() {
        int y = 0;
        for (String s: storyText) {
            for (int i = 0; s.length() > i; ++i) {
                screen.putString(i+4, y+5, s.charAt(i) + "", Terminal.Color.WHITE, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(3);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }

        y = 0;
        Terminal.Color color;
        for (String s: optionStrings) {
            for (int i = 0; s.length() > i; ++i) {
                switch (y){
                    case 0:
                        color = Terminal.Color.GREEN;
                        break;
                    case 1:
                        color = Terminal.Color.YELLOW;
                        break;
                    case 2:
                        color = Terminal.Color.BLUE;
                        break;
                    case 3:
                        color = Terminal.Color.RED;
                        break;
                    default:
                        color = Terminal.Color.MAGENTA;
                }
                screen.putString(i+3, y+20, s.charAt(i) + "", color, Terminal.Color.BLACK);
                screen.refresh();
                try {
                    Thread.sleep(15);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            ++y;
        }
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Easy");
        optionStrings.add("2. Medium");
        optionStrings.add("3. Hard");
        optionStrings.add("4. Ultimate");
        title = "THE COLOSSEUM";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void afterCombat()
    {
        if(Game.character.isAlive() == false){
            screen.putString(3,28,"Click any key to continue.", Terminal.Color.RED, Terminal.Color.BLACK);
            screen.putString(4,23,"You exhausted your attack for current turn", Terminal.Color.BLACK, Terminal.Color.BLACK);
            screen.refresh();
            while(screen.readInput() == null) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        ((Champion)Game.character).unequipHero();
        int expReward = 0;
        switch (enemy.getRank()){
            case EASY:
                expReward = 1;
                break;
            case MEDIUM:
                expReward = 2;
                break;
            case HARD:
                expReward = 3;
                break;
            case ULTIMATE:
                expReward = 4;
                break;
        }
        ((Champion)Game.character).setExperiencePoints(((Champion)Game.character).getExperiencePoints() + expReward);
        ((Champion) Game.character).setMoney(((Champion)Game.character).getMoney() + enemy.getReward());

        final int health = Game.character.getHealth();
        final int perception = Game.character.getPerception();
        final int strength = Game.character.getStrength();
        final int magic = Game.character.getMagic();

        screen.putString(3, 23, "You gained "+enemy.getReward()+" gold!", Terminal.Color.YELLOW, Terminal.Color.BLACK);
        screen.refresh();

        int level = ((Champion)Game.character).checkEXP();
        if(level > 0){
            screen.putString(3, 24, "Your hero gained "+level+" level!!!", Terminal.Color.GREEN, Terminal.Color.BLACK);
            screen.putString(3, 25, "Health       Perception       Strength       Magic (stats when you are unequipped)", Terminal.Color.GREEN, Terminal.Color.BLACK);
            screen.putString(3, 26, health + "             " + perception+ "             " + strength+ "             " + magic + "", Terminal.Color.GREEN, Terminal.Color.BLACK);
            screen.putString(3, 27, "+"+(Game.character.getHealth() - health) + "            +" + (Game.character.getPerception() - perception)+ "            +" + (Game.character.getStrength() - strength)+ "            +" + (Game.character.getMagic() - magic) + "", Terminal.Color.GREEN, Terminal.Color.BLACK);
            screen.putString(3, 28, "Click any key to continue.", Terminal.Color.RED, Terminal.Color.BLACK);
            screen.refresh();
            while(screen.readInput() == null){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }else {
            screen.putString(3, 28, "Click any key to continue.", Terminal.Color.RED, Terminal.Color.BLACK);
            screen.refresh();
            while(screen.readInput() == null){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected Boolean stayHere(Key key)
    {
        if(key==null)
            return true;
        EnemyFactory enemyFactory = new EnemyFactory();
        switch (key.getCharacter())
        {
            case '1':
                enemy = enemyFactory.createEasyEnemy();
                break;

            case '2':
                enemy = enemyFactory.createMediumEnemy();
                break;

            case '3':
                enemy = enemyFactory.createHardEnemy();
                break;

            case '4':
                enemy = enemyFactory.createUltimateEnemy();
                break;

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        combat();
        afterCombat();

        return false;
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(Game.character.isAlive() == false)
        {
            GameMusic.getGameMusic().changeMusic(GameMusic.deathTheme);
            return new Die();
        }

        if(enemy.getRank() == ERank.ULTIMATE){
            return new KingOfTheArena();
        }
        GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
        return new Scene6();
    }


    public void combat(){
        int Turn = 0;
        Random Throw = new Random();
        IAlterString reactionString;
        IAlterString sequenceString;
        IAlterString rangedPhase = new SequenceString(new AlterString("Range Phase - choose what to do"));
        IAlterString meleePhase = new SequenceString(new AlterString("Melee Phase - choose what to do"));
        IAlterString magicPhase = new SequenceString(new AlterString("Magic Phase - choose what to do"));
        IAlterString attack = new SequenceString(new AlterString("1.Attack"));
        IAlterString defend = new SequenceString(new AlterString("2.Defend"));
        IAlterString onlyDefend = new SequenceString(new AlterString("1.Defend"));

        IAlterString hideAttack = new HideString(new AlterString("1.Attack"));
        IAlterString hideDefend = new HideString(new AlterString("2.Defend"));

        IAlterString over = new SequenceString(new AlterString("Battle is over"));
        IAlterString noAttack = new SequenceString(new AlterString("You exhausted your attack for current turn"));
        IAlterString hideNoAttack = new HideString(new AlterString("You exhausted your attack for current turn"));

        GloriousCombat:
        while(Game.character.isAlive() && enemy.isAlive())
        {
            boolean RangedAttack = false; //w jaki sposob bedzie pobierane info o wyborze trzeba bedzie wstawic
            boolean MeleeAttack = false;
            boolean MagicAttack = false;
            boolean Attacked = false;

            //czyszczenie ekranu
            if(Turn ==0) //co ture raz trzeba tylko czyścic ekran - !!!polepszyc potem!!!
            {
                hideStrings();
                screen.refresh();
                Turn++;
                int y = 0;

                //Wyswietlanie info o potworze - chyba tylko w 1 rundzie zobaczy sie
                for (String s: enemy.getDescription()) {
                    for (int i = 0; s.length() > i; ++i) {
                        screen.putString(i+4, y+5, s.charAt(i) + "", Terminal.Color.WHITE, Terminal.Color.BLACK);
                        screen.refresh();
                        try {
                            Thread.sleep(3);
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                    }
                    ++y;
                }
            }

            screen.putString(3,11,"Turn: "+String.valueOf(Turn),Terminal.Color.WHITE, Terminal.Color.BLACK);
            //wypisywanie hp
            showHP();
            showStats();

            //wybór atak vs obrona - ranged
            if(!RangedAttack)
            {
                hideNoAttack.print(4,23);
                rangedPhase.print(3,21);
                attack.print(4,22);
                defend.print(4,23);

                Key key;
                boolean waiting = true;
                while(waiting){
                    key = screen.readInput();
                    if(key!=null)
                    {
                        int first = Throw.nextInt(9)+1;
                        int second = Throw.nextInt(9)+1;
                        screen.putString(3,25,
                                "Perception + Throw + Throw = "
                                        +(first+second+Game.character.getPerception())+
                                        "/"
                                        +enemy.getPerception(),
                                Terminal.Color.WHITE, Terminal.Color.BLACK);
                        switch (key.getCharacter()){
                            case '1':
                                if((Game.character.getPerception()+first+second)>=(enemy.getPerception()))
                                {
                                    enemy.setCurrentHealth(enemy.getCurrentHealth()-Game.character.getRangedDamage());
                                    reactionString = new BlinkingString(new AlterString("Successfully dealt "+Game.character.getRangedDamage()+" to "+enemy.getName()));
                                    reactionString.print(3,27);
                                }
                                else
                                {
                                    Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getRangedDamage());
                                    reactionString = new BlinkingString(new AlterString("Attack failed. "+enemy.getName()+" deals "+enemy.getRangedDamage()+" to you"));
                                    reactionString.print(3,27);

                                }
                                waiting = false;
                                Attacked = true;
                                break;

                            case '2':
                                if((Game.character.getPerception()+first+second)>=(enemy.getPerception()))
                                {
                                    //no damage taken
                                    reactionString = new BlinkingString(new AlterString("Defense successfull - no damage taken"));
                                    reactionString.print(3,27);
                                }
                                else
                                {
                                    Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getRangedDamage());
                                    reactionString = new BlinkingString(new AlterString("Defense failed. "+enemy.getName()+" deals "+enemy.getRangedDamage()+" to you"));
                                    reactionString.print(3,27);
                                }
                                waiting = false;
                                break;

                            default:
                                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                                screen.refresh();
                        }
                        screen.putString(3,25,
                                "Perception("+Game.character.getPerception()+") + Random("+first+") + Random("+second+" ="
                                        +(first+second+Game.character.getPerception())+
                                        "/enemy value = "
                                        +enemy.getPerception(),
                                Terminal.Color.BLACK, Terminal.Color.BLACK);
                        screen.refresh();
                    }

                }
                RangedAttack=true;
                hideAttack.print(4,22);
                hideDefend.print(4,23);
            }
            showHP();
            if(!Game.character.isAlive() || !enemy.isAlive()){
                over.print(3,28);
                break GloriousCombat;
            }

            //wybór atak vs obrona - melee - po Ranged
            if(RangedAttack)
            {
                hideNoAttack.print(4,23);
                if(Attacked)
                {
                    meleePhase.print(3,21);
                    onlyDefend.print(4,22);
                    noAttack.print(4,23);
                }
                else
                {
                    meleePhase.print(3,21);
                    attack.print(4,22);
                    defend.print(4,23);
                }
                screen.refresh();

                Key key;
                boolean waiting = true;
                while(waiting){
                    key = screen.readInput();
                    if(key!=null)
                    {
                        int first = Throw.nextInt(9)+1;
                        int second = Throw.nextInt(9)+1;
                        screen.putString(3,25,
                                "Strength + Throw + Throw = "
                                        +(first+second+Game.character.getStrength())+
                                        "/"
                                        +enemy.getStrength(),
                                Terminal.Color.WHITE, Terminal.Color.BLACK);
                        switch (key.getCharacter()){
                            case '1':
                                if(Attacked)
                                {
                                    if((Game.character.getStrength()+first+second)>=(enemy.getStrength()))
                                    {
                                        //no damage taken
                                        reactionString = new BlinkingString(new AlterString("Defense successfull - no damage taken"));
                                        reactionString.print(3,27);
                                    }
                                    else
                                    {
                                        Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getMeleeDamage());
                                        reactionString = new BlinkingString(new AlterString("Defense failed. "+enemy.getName()+" deals "+enemy.getMeleeDamage()+" to you"));
                                        reactionString.print(3,27);
                                    }
                                }
                                else
                                {
                                    if((Game.character.getStrength()+first+second)>=(enemy.getStrength()))
                                    {
                                        enemy.setCurrentHealth(enemy.getCurrentHealth()-Game.character.getMeleeDamage());
                                        reactionString = new BlinkingString(new AlterString("Successfully dealt "+Game.character.getMeleeDamage()+" to "+enemy.getName()));
                                        reactionString.print(3,27);
                                    }
                                    else
                                    {
                                        Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getMeleeDamage());
                                        reactionString = new BlinkingString(new AlterString("Attack failed. "+enemy.getName()+" deals "+enemy.getMeleeDamage()+" to you"));
                                        reactionString.print(3,27);
                                    }
                                }
                                waiting = false;
                                Attacked = true;
                                break;

                            case '2':
                                if(Attacked)
                                {
                                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                                    screen.refresh();
                                }
                                else
                                {
                                    if((Game.character.getStrength()+first+second)>=(enemy.getStrength()))
                                    {
                                        //no damage taken
                                        reactionString = new BlinkingString(new AlterString("Defense successfull - no damage taken"));
                                        reactionString.print(3,27);
                                    }
                                    else
                                    {
                                        Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getMeleeDamage());
                                        reactionString = new BlinkingString(new AlterString("Defense failed. "+enemy.getName()+" deals "+enemy.getMeleeDamage()+" to you"));
                                        reactionString.print(3,27);
                                    }
                                    waiting = false;
                                }
                                break;

                            default:
                                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                                screen.refresh();
                        }
                        screen.putString(3,25,
                                "Strength + Throw + Throw = "
                                        +(first+second+Game.character.getStrength())+
                                        "/"
                                        +enemy.getStrength(),
                                Terminal.Color.BLACK, Terminal.Color.BLACK);
                        screen.refresh();
                    }

                }
                MeleeAttack=true;
                hideAttack.print(4,22);
                hideDefend.print(4,23);
            }
            showHP();
            if(!Game.character.isAlive() || !enemy.isAlive()){
                over.print(3,28);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                break GloriousCombat;
            }

            //wybór atak vs obrona - magic - po Melee
            if(MeleeAttack)
            {
                hideNoAttack.print(4,23);
                if(Attacked)
                {
                    magicPhase.print(3,21);
                    onlyDefend.print(4,22);
                    noAttack.print(4,23);
                }
                else
                {
                    magicPhase.print(3,21);
                    attack.print(4,22);
                    defend.print(4,23);
                }
                screen.refresh();

                Key key;
                boolean waiting = true;
                while(waiting){
                    key = screen.readInput();
                    if(key!=null)
                    {
                        int first = Throw.nextInt(9)+1;
                        int second = Throw.nextInt(9)+1;
                        screen.putString(3,25,
                                "Magic + Throw + Throw = "
                                        +(first+second+Game.character.getMagic())+
                                        "/"
                                        +enemy.getMagic(),
                                Terminal.Color.WHITE, Terminal.Color.BLACK);
                        switch (key.getCharacter()){
                            case '1':
                                if(Attacked)
                                {
                                    if((Game.character.getMagic()+first+second)>=(enemy.getMagic()))
                                    {
                                        //no damage taken
                                        reactionString = new BlinkingString(new AlterString("Defense successfull - no damage taken"));
                                        reactionString.print(3,27);
                                    }
                                    else
                                    {
                                        Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getMagicDamage());
                                        reactionString = new BlinkingString(new AlterString("Defense failed. "+enemy.getName()+" deals "+enemy.getMagicDamage()+" to you"));
                                        reactionString.print(3,27);
                                    }
                                }
                                else
                                {
                                    if((Game.character.getMagic()+first+second)>=(enemy.getMagic()))
                                    {
                                        enemy.setCurrentHealth(enemy.getCurrentHealth()-Game.character.getMagicDamage());
                                        reactionString = new BlinkingString(new AlterString("Successfully dealt "+Game.character.getMagicDamage()+" to "+enemy.getName()));
                                        reactionString.print(3,27);
                                    }
                                    else
                                    {
                                        Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getMagicDamage());
                                        reactionString = new BlinkingString(new AlterString("Attack failed. "+enemy.getName()+" deals "+enemy.getMagicDamage()+" to you"));
                                        reactionString.print(3,27);
                                    }
                                }
                                waiting = false;
                                Attacked = true;
                                break;

                            case '2':
                                if(Attacked)
                                {
                                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                                    screen.refresh();
                                }
                                else
                                {
                                    if((Game.character.getMagic()+first+second)>=(enemy.getMagic()))
                                    {
                                        //no damage taken
                                        reactionString = new BlinkingString(new AlterString("Defense successfull - no damage taken"));
                                        reactionString.print(3,27);
                                    }
                                    else
                                    {
                                        Game.character.setCurrentHealth(Game.character.getCurrentHealth()-enemy.getMagicDamage());
                                        reactionString = new BlinkingString(new AlterString("Defense failed. "+enemy.getName()+" deals "+enemy.getMagicDamage()+" to you"));
                                        reactionString.print(3,27);
                                    }
                                    waiting = false;
                                }
                                break;

                            default:
                                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                                screen.refresh();
                        }
                        screen.putString(3,25,
                                "Magic + Throw + Throw = "
                                        +(first+second+Game.character.getMagic())+
                                        "/"
                                        +enemy.getMagic(),
                                Terminal.Color.BLACK, Terminal.Color.BLACK);
                        screen.refresh();
                    }

                }
                MagicAttack=true;
                hideAttack.print(4,22);
                hideDefend.print(4,23);
            }
            showHP();
            if(!Game.character.isAlive() || !enemy.isAlive()){
                over.print(3,28);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                break GloriousCombat;
            }


            if(MagicAttack){
                Turn++;
            }

        }
    }
}
