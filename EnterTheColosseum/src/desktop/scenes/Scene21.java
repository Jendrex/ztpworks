package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;
import desktop.scenes.menu.Menu;

import java.io.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Scene21 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene1/story_text.txt";

    public Scene21(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any key to continue");
        title = "NOT WHAT I WAS HOPING FOR...";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            reset();
            GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
            return new Menu();
        }
        return null;
    }
}
