package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class AlternativeEnding extends Scene {
    private final String STORY_TEXT_PATH = "files/alternativeending/story_text.txt";

    public AlternativeEnding(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any key to continue");
        title = "OUT OF THE PRISON";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            System.exit(0);
        }
        return null;
    }

}
