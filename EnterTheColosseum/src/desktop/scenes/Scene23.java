package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Scene23 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene23/story_text.txt";

    public Scene23() {
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Yes. <run back to Crossroads as fast as you can dropping soap on the way>");
        optionStrings.add("2. No - who the hell do you think you are?!");
        title = "CLUTCH MOMENT";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()) {
            case '1':
                MagicalSoap = false;
                GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
                return new Scene8();

            case '2':
                GameMusic.getGameMusic().changeMusic(GameMusic.deathTheme);
                return new Scene21();
        }
        return null;
    }
}
