package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene17 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene17/story_text.txt";
    private final String STORY_TEXT_PATH_NOSOAP = "files/scene17/story_text_nosoap.txt";

    public Scene17() {
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        if (MagicalSoap) {
            optionStrings.add("1. A mole on the way gave it to me");
            optionStrings.add("2. Who cares... holy s*** you are hot as f*ck <staring intensifies>");
            optionStrings.add("3. <remain silent>");
            title = "BEAUTIFUL SURPRISE";

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
            try {
                String line;
                while ((line = readLine(is)) != null){
                    storyText.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            optionStrings.add("1. Oh yea? Well f*ck you too!! Fuckin' bitch...<leave>");
            optionStrings.add("2. *sigh* Welp... guess I have no business here <leave>");
            title = "AN OLD HAG";

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH_NOSOAP);
            try {
                String line;
                while ((line = readLine(is)) != null){
                    storyText.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected Scene nextScene(Key key)
    {
        if (MagicalSoap) {
            switch (key.getCharacter()) {
                case '1':
                    return new Scene18();

                case '2':
                    return new Scene19();

                case '3':
                    return new Scene20();

                default:
                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                    screen.refresh();
            }
        }
        else{
            switch (key.getCharacter()) {
                case '1':
                    GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
                    return new Scene8();

                case '2':
                    GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
                    return new Scene8();

                default:
                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                    screen.refresh();
            }
        }

        return null;
    }
}
