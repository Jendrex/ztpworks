package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Scene25 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene25/story_text.txt";

    public Scene25(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Oh hell yea!");
        optionStrings.add("2. I'm sorry but I have to gain strength to become arena Champion. My duties are elsewhere");
        optionStrings.add("3. Nah I'm fine thank you <gives soap> <goes back to Crossroads>");
        title = "LUCRATIVE PROPOSITION";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                return new Scene24();

            case '2':
                return new Scene26();

            case '3':
                MagicalSoap = false;
                GameMusic.getGameMusic().changeMusic(GameMusic.mainTheme);
                return new Scene8();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
