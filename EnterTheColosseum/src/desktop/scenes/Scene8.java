package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene8 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene8/story_text.txt";

    public Scene8(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Turn right.");
        optionStrings.add("2. Turn left.");
        optionStrings.add("3. Go back to the Hallway.");
        title = "CROSSROADS";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                return new Scene9();

            case '2':
                return new Scene10();

            case '3':
                return new Scene6();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
