package desktop.scenes;

import java.io.*;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.characters.heroes.Champion;
import desktop.items.RealWeapon;
import desktop.main.Game;

/**
 * Created by Sparrow on 20.10.16.
 */
public class Scene2 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene2/story_text.txt";
    private final String STORY_TEXT_PATH_VISITED = "files/scene2/story_text_visited.txt";
    public static boolean visited;

    public Scene2(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        if(!visited){
            optionStrings.add("1. Yes!!! Old sword(3): 1/0 1/0 1/0");
            optionStrings.add("2. No...");
        }
        else {
            optionStrings.add("Press any button to continue.");
        }
        title = "CLEANING THE ROOM.";

        String path;
        path = visited ? STORY_TEXT_PATH_VISITED : STORY_TEXT_PATH;

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(path);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(!visited) {
            switch (key.getCharacter()) {
                case '1':
                    RealWeapon weapon = new RealWeapon.Builder()
                            .name("Iron sword")
                            .description("Old iron sword")
                            .cost(3)
                            .strengthMod(1)
                            .meleeDamageMod(0)
                            .perceptionMod(1)
                            .rangedDamageMod(0)
                            .magicMod(1)
                            .magicDamageMod(0)
                            .build();
                    ((Champion) Game.character).setEquippedWeapon(weapon);
                    break;

                case '2':
                    break;

                default:
                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                    screen.refresh();
            }
            visited = true;
        }

        return new Scene1();
    }

}
