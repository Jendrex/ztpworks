package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.characters.heroes.Champion;
import desktop.items.Armor;
import desktop.items.RealArmor;
import desktop.items.RealWeapon;
import desktop.items.Weapon;
import desktop.main.Game;
import desktop.tools.ItemFactory;

import java.io.*;

import static java.lang.Thread.sleep;

/**
 * Created by jedrek506 on 06.11.16.
 */
public class Shop extends Scene{

    private final String STORY_TEXT_PATH = "files/shop/story_text.txt";
    private String HeadArmorString = "Name                     Cost  HP-Boost  Description";
    private String HeadWeaponString = "Name                     Cost Strength MeleeDam Perception RangedDam Magic MagicDam  Description";
    ItemFactory items = new ItemFactory();

    public Shop()
    {
        super();
        setStrings();
    }

    @Override
    protected void setStrings() {
        title = "SHOP";
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void smallSleep()
    {
        try {
            sleep(25);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        screen.refresh();
    }

    private void showOptions()
    {
        screen.putString(3,10,"Choose equipment you are interested in:", Terminal.Color.WHITE, Terminal.Color.BLACK);
        screen.putString(4,11,"1.Armors", Terminal.Color.BLUE, Terminal.Color.BLACK);
        screen.putString(4,12,"2.Weapons", Terminal.Color.RED, Terminal.Color.BLACK);
        screen.putString(4,13,"0.Quit", Terminal.Color.CYAN, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void hideOptions()
    {
        screen.putString(3,10,"Choose equipment you are interested in:", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.putString(4,11,"1.Armors", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.putString(4,12,"2.Weapons", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.putString(4,13,"0.Quit", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void printArmor(int x, int y, RealArmor armor, Terminal.Color color)
    {
        screen.putString(2,y,x+"."+armor.getName(), color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(29,y,armor.getCost()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(35,y,"+"+armor.getHpMod(), color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(45,y,armor.getDescription(), color, Terminal.Color.BLACK);
        smallSleep();
    }

    private void printWeapon(int x, int y, RealWeapon weapon, Terminal.Color color)
    {
        screen.putString(2, y,x+"."+weapon.getName(), color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(29, y, weapon.getCost()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(34, y, weapon.getStrengthMod()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(43, y, weapon.getMeleeDamageMod()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(52, y, weapon.getPerceptionMod()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(63, y, weapon.getRangedDamageMod()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(73, y, weapon.getMagicMod()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(79, y, weapon.getMagicDamageMod()+"", color, Terminal.Color.BLACK);
        smallSleep();
        screen.putString(89, y, weapon.getDescription(), color, Terminal.Color.BLACK);
        smallSleep();
    }

    private void hideArmor(int x, int y, RealArmor armor, Terminal.Color color)
    {
        screen.putString(2,y,x+"."+armor.getName(), color, Terminal.Color.BLACK);
        screen.putString(29,y,armor.getCost()+"", color, Terminal.Color.BLACK);
        screen.putString(35,y,"+"+armor.getHpMod(), color, Terminal.Color.BLACK);
        screen.putString(45,y,armor.getDescription(), color, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void hideWeapon(int x, int y, RealWeapon weapon, Terminal.Color color)
    {
        screen.putString(2, y,x+"."+weapon.getName(), color, Terminal.Color.BLACK);
        screen.putString(29, y, weapon.getCost()+"", color, Terminal.Color.BLACK);
        screen.putString(34, y, weapon.getStrengthMod()+"", color, Terminal.Color.BLACK);
        screen.putString(43, y, weapon.getMeleeDamageMod()+"", color, Terminal.Color.BLACK);
        screen.putString(52, y, weapon.getPerceptionMod()+"", color, Terminal.Color.BLACK);
        screen.putString(63, y, weapon.getRangedDamageMod()+"", color, Terminal.Color.BLACK);
        screen.putString(73, y, weapon.getMagicMod()+"", color, Terminal.Color.BLACK);
        screen.putString(79, y, weapon.getMagicDamageMod()+"", color, Terminal.Color.BLACK);
        screen.putString(89, y, weapon.getDescription(), color, Terminal.Color.BLACK);
        screen.refresh();
    }
    private void showArmors(Terminal.Color color)
    {
        Armor currentArmor = ((Champion) Game.character).getEquippedArmor();
        if(currentArmor!=null) {
            screen.putString(3,13,"Current armor: "+currentArmor.getName(), color, Terminal.Color.BLACK);
        }
        else{
            screen.putString(3,13,"No armor equipped ", color, Terminal.Color.BLACK);
        }

        screen.putString(3,15,HeadArmorString, color, Terminal.Color.BLACK);

        int x = 1;
        int y = 16;
        for (RealArmor a : items.ArmorList)
        {
            printArmor(x,y,a, color);
            x++;
            y+=2;
        }
        screen.putString(x,y,"0.Exit", color, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void showWeapons(Terminal.Color color)
    {
        Weapon currentWeapon = ((Champion)Game.character).getEquippedWeapon();
        if(currentWeapon!=null) {
            screen.putString(3,13,"Current weapon: "+currentWeapon.getName(), color, Terminal.Color.BLACK);
        }
        else{
            screen.putString(3,13,"No weapon equipped ", color, Terminal.Color.BLACK);
        }
        screen.putString(3,15,HeadWeaponString, color, Terminal.Color.BLACK);

        int x = 1;
        int y = 16;
        for(RealWeapon w : items.WeaponList)
        {
            printWeapon(x,y,w, color);
            x++;
            y+=2;
        }
        screen.putString(x,y,"0.Exit", color, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void hideArmors()
    {
        screen.putString(3,15,HeadArmorString, Terminal.Color.BLACK, Terminal.Color.BLACK);

        int x = 1;
        int y = 16;
        for (RealArmor a : items.ArmorList)
        {
            hideArmor(x,y,a, Terminal.Color.BLACK);
            x++;
            y+=2;
        }
        screen.putString(3,13,"                                       ", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.putString(x,y,"0.Exit", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void hideWeapons()
    {
        screen.putString(3,15,HeadWeaponString, Terminal.Color.BLACK, Terminal.Color.BLACK);

        int x = 1;
        int y = 16;
        for(RealWeapon w : items.WeaponList)
        {
            hideWeapon(x,y,w, Terminal.Color.BLACK);
            x++;
            y+=2;
        }
        screen.putString(3,13,"                                      ", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.putString(x,y,"0.Exit", Terminal.Color.BLACK, Terminal.Color.BLACK);
        screen.refresh();
    }

    private void buyArmor(int number)
    {
        screen.putString(3,38, WRONG_OPTION, Terminal.Color.BLACK, Terminal.Color.BLACK);
        RealArmor armor = items.ArmorList.get(number);
        if(armor==null)
            return;

        if(armor.equals(((Champion)Game.character).getEquippedArmor())){
            screen.putString(3,38,"You already own this armor", Terminal.Color.RED, Terminal.Color.BLACK);
            screen.refresh();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.putString(3,38,"You already own this armor", Terminal.Color.BLACK, Terminal.Color.BLACK);
            screen.refresh();
            return;
        }

        if(armor.getCost()>((Champion)Game.character).getMoney())
        {
            screen.putString(3,38,"You can't afford this item", Terminal.Color.RED, Terminal.Color.BLACK);
            screen.refresh();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.putString(3,38,"You can't afford this item", Terminal.Color.BLACK, Terminal.Color.BLACK);
            screen.refresh();
        }
        else
        {
            screen.putString(3,9,"Your money: "+ ((Champion)Game.character).getMoney(), Terminal.Color.BLACK, Terminal.Color.BLACK);
            ((Champion)Game.character).setMoney(((Champion)Game.character).getMoney()-armor.getCost());
            screen.putString(3,9,"Your money: "+ ((Champion)Game.character).getMoney(), Terminal.Color.GREEN, Terminal.Color.BLACK);
            ((Champion)Game.character).setEquippedArmor(armor);
            screen.putString(3,38,"You bought "+armor.getName()+ " and equipped it", Terminal.Color.GREEN, Terminal.Color.BLACK);
            screen.refresh();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.putString(3,13,"Current armor: "+armor.getName(), Terminal.Color.WHITE, Terminal.Color.BLACK);
            screen.putString(3,38,"You bought "+armor.getName()+ " and equipped it", Terminal.Color.BLACK, Terminal.Color.BLACK);
            screen.refresh();
        }
    }

    private void buyWeapon(int number)
    {
        screen.putString(3,38, WRONG_OPTION, Terminal.Color.BLACK, Terminal.Color.BLACK);
        RealWeapon weapon = items.WeaponList.get(number);
        if(weapon==null)
            return;

        if(weapon.equals(((Champion)Game.character).getEquippedWeapon())){
            screen.putString(3,38,"You already own this weapon", Terminal.Color.RED, Terminal.Color.BLACK);
            screen.refresh();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.putString(3,38,"You already own this weapon", Terminal.Color.BLACK, Terminal.Color.BLACK);
            screen.refresh();
            return;
        }

        if(weapon.getCost()>((Champion)Game.character).getMoney())
        {
            screen.putString(3,38,"You can't afford this item", Terminal.Color.RED, Terminal.Color.BLACK);
            screen.refresh();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.putString(3,38,"You can't afford this item", Terminal.Color.BLACK, Terminal.Color.BLACK);
            screen.refresh();
        }
        else
        {
            screen.putString(3,9,"Your money: "+ ((Champion)Game.character).getMoney(), Terminal.Color.BLACK, Terminal.Color.BLACK);
            ((Champion)Game.character).setMoney(((Champion)Game.character).getMoney()-weapon.getCost());
            screen.putString(3,9,"Your money: "+ ((Champion)Game.character).getMoney(), Terminal.Color.GREEN, Terminal.Color.BLACK);
            ((Champion)Game.character).setEquippedWeapon(weapon);
            screen.putString(3,38,"You bought "+weapon.getName()+ " and equipped it", Terminal.Color.GREEN, Terminal.Color.BLACK);
            screen.refresh();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.putString(3,13,"Current weapon: "+weapon.getName()+"         ", Terminal.Color.WHITE, Terminal.Color.BLACK);
            screen.putString(3,38,"You bought "+weapon.getName()+ " and equipped it", Terminal.Color.BLACK, Terminal.Color.BLACK);
            screen.refresh();
        }
    }


    @Override
    protected Boolean stayHere(Key key)
    {
        shop();
        return false;
    }

    @Override
    protected Scene nextScene(Key key)
    {
        return new Scene6();
    }

    private void shop()
    {
        Key key;
        Key key2;
        Key key3;
        boolean getOut = false;

        while(!getOut)
        {
            showOptions();
            key = screen.readInput();
            if(key!=null)
            {
                switch(key.getCharacter())
                {
                    case '1':
                        hideOptions();
                        screen.putString(3,9,"Your money: "+ ((Champion)Game.character).getMoney(), Terminal.Color.GREEN, Terminal.Color.BLACK);
                        hideWeapons();
                        showArmors(Terminal.Color.WHITE);

                        boolean armors = true;
                        while(armors)
                        {
                            key2 = screen.readInput();
                            if(key2!=null)
                            {
                                switch(key2.getCharacter())
                                {
                                    case '1':
                                        buyArmor(0);
                                        break;
                                    case '2':
                                        buyArmor(1);
                                        break;
                                    case '3':
                                        buyArmor(2);
                                        break;
                                    case '4':
                                        buyArmor(3);
                                        break;
                                    case '5':
                                        buyArmor(4);
                                        break;
                                    case '0':
                                        armors = false;
                                        hideArmors();
                                        break;
                                    default:
                                        screen.putString(3,38, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                                        screen.refresh();
                                }
                            }
                        }
                        break;

                    case '2':
                        hideOptions();
                        screen.putString(3,9,"Your money: "+ ((Champion)Game.character).getMoney(), Terminal.Color.GREEN, Terminal.Color.BLACK);
                        hideArmors();
                        showWeapons(Terminal.Color.WHITE);

                        boolean weapons = true;
                        while(weapons)
                        {
                            key3 = screen.readInput();
                            if(key3!=null)
                            {
                                switch(key3.getCharacter())
                                {
                                    case '1':
                                        buyWeapon(0);
                                        break;
                                    case '2':
                                        buyWeapon(1);
                                        break;
                                    case '3':
                                        buyWeapon(2);
                                        break;
                                    case '4':
                                        buyWeapon(3);
                                        break;
                                    case '5':
                                        buyWeapon(4);
                                        break;
                                    case '6':
                                        buyWeapon(5);
                                        break;
                                    case '7':
                                        buyWeapon(6);
                                        break;
                                    case '8':
                                        buyWeapon(7);
                                        break;
                                    case '0':
                                        weapons=false;
                                        hideWeapons();
                                        break;
                                    default:
                                        screen.putString(3, 38, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                                        screen.refresh();
                                }
                            }
                        }
                        break;

                    case '0':
                        getOut=true;
                        break;
                    default:
                        screen.putString(3, 38, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                        screen.refresh();

                }

            }
        }
    }
}
