package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Sparrow on 20.10.16.
 */
public class Scene3 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene3/story_text.txt";

    public Scene3(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any button to continue.");
        title = "LOOKING AT YOUR ROOM";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            return new Scene1();
        }
        return null;
    }

}
