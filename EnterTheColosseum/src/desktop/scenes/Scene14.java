package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.main.Game;
import desktop.main.GameMusic;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene14 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene14/story_text.txt";

    public Scene14(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Ignore the hunch and go forward.");
        optionStrings.add("2. Use the shortcut to the hallway.");
        title = "HUNCH";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                if (Game.character.getStrength() < 5)
                    GameMusic.getGameMusic().changeMusic(GameMusic.deathTheme);
                return new Scene16();

            case '2':
                return new Scene6();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }
}
