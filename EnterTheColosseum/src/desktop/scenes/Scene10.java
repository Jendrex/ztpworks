package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.*;

/**
 * Created by jedrek506 on 11.11.16.
 */
public class Scene10 extends Scene{
    private final String STORY_TEXT_PATH = "files/scene10/story_text.txt";
    private final String STORY_TEXT_PATH_VISITED = "files/scene10/story_text_visited.txt";


    public Scene10(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        if(!moleVisited)
        {
            optionStrings.add("1. Yes.");
            optionStrings.add("2. No.");
            title = "AN OLD MOLE";

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
            try {
                String line;
                while ((line = readLine(is)) != null){
                    storyText.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            optionStrings.add("1. Go forward (to the crystal cave)");
            optionStrings.add("2. Go back");
            title = "AN OLD MOLE (NOT ANYMORE)";

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH_VISITED);
            try {
                String line;
                while ((line = readLine(is)) != null){
                    storyText.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(!moleVisited) {
            switch (key.getCharacter()){
                case '1':
                    MagicalSoap = true;
                    moleVisited = true;
                    return new Scene12();

                case '2':
                    moleVisited = true;
                    return new Scene11();

                default:
                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                    screen.refresh();
            }
        }
        else
        {
            switch (key.getCharacter()){
                case '1':
                    return new Scene14();

                case '2':
                    return new Scene8();

                default:
                    screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                    screen.refresh();
            }
        }
        return null;
    }

}
