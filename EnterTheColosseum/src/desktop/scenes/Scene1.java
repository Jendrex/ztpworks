package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;
import desktop.characters.heroes.Champion;
import desktop.main.Game;

import java.io.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Scene1 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene1/story_text.txt";

    public Scene1(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("1. Clean up your room.");
        optionStrings.add("2. Look around");
        optionStrings.add("3. Take a rest for 1 gold coin. (Currently you have " + ((Champion)Game.character).getMoney() + " gold coins)");
        optionStrings.add("4. Go out of your room.");
        title = "NEW BEGINNING";
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Scene nextScene(Key key)
    {
        switch (key.getCharacter()){
            case '1':
                return new Scene2();
            case '2':
                return new Scene3();

            case '3':
                if(((Champion)Game.character).getMoney() <= 0){
                    screen.putString(3, 28, "You don't have enough money to take a rest.", Terminal.Color.RED, Terminal.Color.BLACK);
                    screen.refresh();
                    break;
                }
                ((Champion)Game.character).setMoney(((Champion)Game.character).getMoney() - 1);
                return new Scene4();

            case '4':
                return new Scene6();

            default:
                screen.putString(3, 30, WRONG_OPTION, Terminal.Color.RED, Terminal.Color.BLACK);
                screen.refresh();
        }
        return null;
    }

}
