package desktop.scenes;

import com.googlecode.lanterna.input.Key;
import desktop.main.Game;

import java.io.*;

/**
 * Created by sparrow on 10.11.16.
 */
public class Scene4 extends Scene {
    private final String STORY_TEXT_PATH = "files/scene4/story_text.txt";

    public Scene4(){
        super();
        setStrings();
    }

    @Override
    public void setStrings() {
        optionStrings.add("Press any button to continue.");
        title = "SLEEP NOW, MY DEAR...";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(STORY_TEXT_PATH);
        try {
            String line;
            while ((line = readLine(is)) != null){
                storyText.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scene nextScene(Key key)
    {
        if(key!=null){
            return new Scene1();
        }
        return null;
    }

}
