package desktop.windows;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Component;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.*;
import com.googlecode.lanterna.gui.layout.LinearLayout;
import com.googlecode.lanterna.gui.layout.VerticalLayout;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.swing.*;

/**
 * Created by Sparrow on 18.10.16.
 */
public class MainWindow {
    private static MainWindow mainWindow = null;
    private Screen screen;
    private SwingTerminal swingTerminal;

    private MainWindow(){
        //screen = TerminalFacade.createScreen();
        //screen.startScreen();

        swingTerminal = new SwingTerminal();
        screen = new Screen(swingTerminal);
        screen.startScreen();
        swingTerminal.getJFrame().addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                screen.stopScreen();
                System.exit(0);
            }
        });
    }

    public Screen getScreen() {
        return screen;
    }

    public static MainWindow getMainWindow(){
        if(mainWindow == null){
            mainWindow = new MainWindow();
        }
        return mainWindow;
    }

}
