package desktop.characters.enemies;


import desktop.characters.Character;

import java.util.ArrayList;

/**
 * Created by sparrow on 18.10.16.
 */

public class Enemy extends Character {
    private ERank rank; //trudnosc pokonania
    private int reward; //pieniądze
    private ArrayList<String> description;

    private Enemy(final Builder builder){
        super();
        this.health = builder.health;
        this.currentHealth = builder.currentHealth;
        this.perception = builder.perception;
        this.strength = builder.strength;
        this.magic = builder.magic;
        this.rangedDamage = builder.rangedDamage;
        this.meleeDamage = builder.meleeDamage;
        this.magicDamage = builder.magicDamage;
        this.name = builder.name;
        this.rank = builder.rank;
        this.reward = builder.reward;
        this.description = builder.descripion;
    }

    public ERank getRank() {
        return rank;
    }

    public void setRank(ERank rank) {
        this.rank = rank;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public ArrayList<String> getDescription() {
        return description;
    }

    public static class Builder{
        private int health, currentHealth;
        private int perception;
        private int strength;
        private int magic;
        private int rangedDamage;
        private int meleeDamage;
        private int magicDamage;
        private String name;
        private ERank rank;
        private int reward;
        private ArrayList<String> descripion;

        public Builder health(final int health){
            this.health = health;
            return this;
        }

        public Builder currentHealth(final int currentHealth){
            this.currentHealth = currentHealth;
            return this;
        }

        public Builder perception(final int perception){
            this.perception = perception;
            return this;
        }

        public Builder strength(final int strength){
            this.strength = strength;
            return this;
        }

        public Builder magic(final int magic){
            this.magic = magic;
            return this;
        }

        public Builder rangedDamage(final int rangedDamage){
            this.rangedDamage = rangedDamage;
            return this;
        }

        public Builder meleeDamage(final int meleeDamage){
            this.meleeDamage = meleeDamage;
            return this;
        }

        public Builder magicDamage(final int magicDamage){
            this.magicDamage = magicDamage;
            return this;
        }

        public Builder name(final String name){
            this.name = name;
            return this;
        }

        public Builder rank(final ERank rank){
            this.rank = rank;
            return this;
        }

        public Builder revard(final int reward){
            this.reward = reward;
            return this;
        }

        public Builder description(final ArrayList<String> descripion){
            this.descripion = descripion;
            return this;
        }

        public Enemy build(){
            return new Enemy(this);
        }
    }
}
