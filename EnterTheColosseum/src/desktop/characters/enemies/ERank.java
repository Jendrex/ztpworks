package desktop.characters.enemies;

/**
 * Created by sparrow on 18.10.16.
 */
public enum ERank {
    EASY,
    MEDIUM,
    HARD,
    ULTIMATE
}
