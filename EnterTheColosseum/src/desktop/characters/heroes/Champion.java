package desktop.characters.heroes;

import desktop.characters.Character;
import desktop.items.Armor;
import desktop.items.RealArmor;
import desktop.items.RealWeapon;
import desktop.items.Weapon;
import desktop.main.Game;

import java.util.Random;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Champion extends Character {
    private int experiencePoints;
    private int money;
    private Weapon equippedWeapon;
    private Armor equippedArmor;

    public Champion(String name){
        super();

        health = 5;
        currentHealth = 5;
        perception = 4;
        strength = 4;
        magic = 2;
        rangedDamage = 1;
        meleeDamage = 2;
        magicDamage = 1;
        experiencePoints = 0;
        money = 3;
        if(name.isEmpty()){
            this.name = "Joe Fire";
        }
        this.name = name;
    }

    public void equipHero(){ //na poczatku walki, aby uzbroic bohatera (bohater moze byc zaskoczony i nieprzygotowany do walki)
        if(!((Champion) Game.character).getEquippedWeapon().isNull()){
            equippedWeapon.addBonus();
        }
        if(!((Champion)Game.character).getEquippedArmor().isNull()){
            equippedArmor.addBonus();
        }
    }

    public void unequipHero(){ //na koncu walki, aby rozbroic bohatera
        if(!((Champion)Game.character).getEquippedWeapon().isNull()){
            equippedWeapon.removeBonus();
        }
        if(!((Champion)Game.character).getEquippedArmor().isNull()){
            equippedArmor.removeBonus();
        }
    }

    public int checkEXP(){
        int gainedLevel = 0;
        while(experiencePoints-3 >= 0){
            experiencePoints-=3;
            levelUp();
            gainedLevel++;
        }
        return gainedLevel;
    }

    private void levelUp(){
        Random generator = new Random();
        for(int i=0; i<2; i++){
            int mod = generator.nextInt(9)+1;
            if(mod < 4){
                perception+=1;
            }else if(mod < 7){
                strength+=1;
            }else if(mod != 10){
                magic+=1;
            }else{
                health+=1;
            }
        }

    }

    public Weapon getEquippedWeapon() {
        return equippedWeapon;
    }

    public void setEquippedWeapon(Weapon equippedWeapon) {
        this.equippedWeapon = equippedWeapon;
    }

    public Armor getEquippedArmor() {
        return equippedArmor;
    }

    public void setEquippedArmor(Armor equippedArmor) {
        this.equippedArmor = equippedArmor;
    }

    public int getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(int experiencePoints) {
        this.experiencePoints = experiencePoints;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
