package desktop.main;

import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by jedrek506 on 14.11.16.
 */
public class GameMusic {
    public static final String mainTheme = "files/sounds/enigma.wav";
    public static final String combatTheme = "files/sounds/unbeatable.wav";
    public static final String deathTheme = "files/sounds/countering-evil.wav";
    public static final String witchTheme = "files/sounds/foggy-woods.wav";
    public AudioPlayer wavPlayer;
    public ContinuousAudioDataStream loop;
    public AudioStream audio;
    private static GameMusic gameMusic = null;

    public static GameMusic getGameMusic()
    {
        if(gameMusic == null)
            gameMusic = new GameMusic();
        return gameMusic;
    }

    public void music(String sound){
        wavPlayer = AudioPlayer.player;
        AudioData audioData;
        loop = null;

        try
        {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream currentTheme = classloader.getResourceAsStream(sound);
            audio = new AudioStream(currentTheme);
            AudioPlayer.player.start(audio);
            audioData = audio.getData();
            loop = new ContinuousAudioDataStream(audioData);

        }
        catch(FileNotFoundException e){
            System.out.print(e.toString());
        }
        catch(IOException error)
        {
            System.out.print(error.toString());
        }
        wavPlayer.start(loop);
    }

    public void changeMusic(String sound)
    {
        wavPlayer.stop(loop);
        music(sound);
    }
}
