package desktop.main;


import desktop.characters.heroes.Champion;
import desktop.items.NullArmor;
import desktop.items.NullWeapon;
import desktop.scenes.Scene;
import desktop.scenes.menu.Menu;
import desktop.windows.MainWindow;
import desktop.characters.Character;
import sun.audio.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Sparrow on 18.10.16.
 */
public class Game {
    private Scene currentScene;
    public static Character character;
    private MainWindow mainWindow;

    public static void main(String[] args) {
        Game game = new Game();
        game.init();
        game.terminalRun();
    }

    public Game() {
        mainWindow = MainWindow.getMainWindow();
    }

    public void init(){
        currentScene = new Menu();
        GameMusic.getGameMusic().music(GameMusic.mainTheme);
    }

    public void terminalRun(){
        while (true){
            currentScene = currentScene.doAll();
        }
    }

}
