package desktop.tools;

import com.googlecode.lanterna.terminal.Terminal;
import desktop.scenes.Scene;

/**
 * Created by Dom on 2016-12-10.
 */
public class SequenceString extends AlterStringDecorator {

    public SequenceString(IAlterString alterString)
    {
        super(alterString);
    }

    @Override
    public void print(int x, int y)
    {
        Terminal.Color color = Terminal.Color.WHITE;
        if(y==22 || y==28 || y==30)
            color = Terminal.Color.RED;
        if(y==23)
            color = Terminal.Color.BLUE;

        String text = decoratedString.text();
        if(text == "1.Defend")
            color = Terminal.Color.BLUE;
        if(text == "You exhausted your attack for current turn")
            color = Terminal.Color.RED;

        for (int i = 0; text.length() > i; ++i) {
            Scene.screen.putString(x+i, y, text.charAt(i) + "", color, Terminal.Color.BLACK);
            Scene.screen.refresh();
            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
