package desktop.tools;

import com.googlecode.lanterna.terminal.Terminal;

/**
 * Created by Dom on 2016-12-10.
 */
public abstract class AlterStringDecorator implements IAlterString {
    protected IAlterString decoratedString;

    public AlterStringDecorator(IAlterString alterString)
    {
        decoratedString = alterString;
    }

    public String text()
    {
        return decoratedString.text();
    }

    public void switchColor(Terminal.Color color)
    {
        decoratedString.switchColor(color);
    }

    public void print(int x, int y)
    {
        decoratedString.print(x,y);
    }


}
