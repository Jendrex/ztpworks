package desktop.tools;

import com.googlecode.lanterna.terminal.Terminal;
import desktop.scenes.Scene;

/**
 * Created by Dom on 2016-12-10.
 */
public class AlterString implements IAlterString {

    String text;
    Terminal.Color color = Terminal.Color.WHITE;

    public String text()
    {
        return this.text;
    }

    public AlterString(String text)
    {
        this.text = text;
    }

    public void switchColor(Terminal.Color color)
    {
        this.color = color;
    }

    public void print(int x, int y)
    {
        Scene.screen.putString(x,y,text,color,Terminal.Color.BLACK);
        Scene.screen.refresh();
    }
}
