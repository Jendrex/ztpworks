package desktop.tools;

import com.googlecode.lanterna.terminal.Terminal;

/**
 * Created by Dom on 2016-12-10.
 */
public class HideString extends AlterStringDecorator {

    public HideString(IAlterString alterString)
    {
        super(alterString);
    }

    @Override
    public void print(int x, int y)
    {
        decoratedString.switchColor(Terminal.Color.BLACK);
        decoratedString.print(x,y);
    }
}
