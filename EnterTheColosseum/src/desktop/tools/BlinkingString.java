package desktop.tools;

import com.googlecode.lanterna.terminal.Terminal;
import desktop.scenes.Scene;

/**
 * Created by Dom on 2016-12-10.
 */
public class BlinkingString extends AlterStringDecorator {

    public BlinkingString(IAlterString alterString)
    {
        super(alterString);
    }

    @Override
    public void print(int x, int y)
    {
        for(int i=0; i<4; ++i){
            decoratedString.switchColor(Terminal.Color.GREEN);
            decoratedString.print(x,y);
            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            IAlterString hiddenString = new HideString(decoratedString);
            hiddenString.print(x,y);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
