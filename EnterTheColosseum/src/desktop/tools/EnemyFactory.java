package desktop.tools;


import desktop.characters.enemies.ERank;
import desktop.characters.enemies.Enemy;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;


/**
 * Created by sparrow on 29.10.16.
 */
public class EnemyFactory {
    private Enemy enemy;
    private ArrayList<String> enemyDescription;

    private void setEnemyDescription(String path) {
        enemyDescription = new ArrayList<String>();

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(path);
        try {
            String line;
            while ((line = readLine(is)) != null){
                enemyDescription.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readLine(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int c;
        for (c = inputStream.read(); c != '\n' && c != -1 ; c = inputStream.read()) {
            byteArrayOutputStream.write(c);
        }
        if (c == -1 && byteArrayOutputStream.size() == 0) {
            return null;
        }
        String line = byteArrayOutputStream.toString("UTF-8");
        return line;
    }

    public Enemy createEasyEnemy(){
        Random generator = new Random();
        int enemyId = generator.nextInt(3) + 1;

        switch (enemyId){
            case 1:
                setEnemyDescription("files/enemies/easy/giant_beetles.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.EASY)
                        .name("Giant beetles")
                        .currentHealth(2)
                        .health(2)
                        .description(enemyDescription)
                        .perception(10)
                        .rangedDamage(0)
                        .strength(14)
                        .meleeDamage(3)
                        .magic(13)
                        .magicDamage(0)
                        .revard(2)
                        .build();
                break;

            case 2:
                setEnemyDescription("files/enemies/easy/vampire_bats_nest.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.EASY)
                        .name("Vampire bats' nest")
                        .currentHealth(2)
                        .health(2)
                        .description(enemyDescription)
                        .perception(13)
                        .rangedDamage(1)
                        .strength(11)
                        .meleeDamage(1)
                        .magic(11)
                        .magicDamage(0)
                        .revard(1)
                        .build();
                break;

            case 3:
                setEnemyDescription("files/enemies/easy/spectral_imp.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.EASY)
                        .name("Spectral imp")
                        .currentHealth(3)
                        .health(3)
                        .description(enemyDescription)
                        .perception(12)
                        .rangedDamage(0)
                        .strength(12)
                        .meleeDamage(1)
                        .magic(9)
                        .magicDamage(1)
                        .revard(1)
                        .build();
                break;

            case 4:
                setEnemyDescription("files/enemies/easy/cave_troll.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.EASY)
                        .name("Cave troll")
                        .currentHealth(3)
                        .health(3)
                        .description(enemyDescription)
                        .perception(10)
                        .rangedDamage(0)
                        .strength(14)
                        .meleeDamage(2)
                        .magic(11)
                        .magicDamage(0)
                        .revard(2)
                        .build();
                break;
        }

        return enemy;
    }

    public Enemy createMediumEnemy(){

        Random generator = new Random();
        int enemyId = generator.nextInt(2) + 1;

        switch (enemyId){
            case 1:
                setEnemyDescription("files/enemies/medium/hungry_corpses.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.MEDIUM)
                        .name("Hungry corpses")
                        .currentHealth(3)
                        .health(3)
                        .description(enemyDescription)
                        .perception(11)
                        .rangedDamage(1)
                        .strength(13)
                        .meleeDamage(2)
                        .magic(16)
                        .magicDamage(1)
                        .revard(3)
                        .build();
                break;

            case 2:
                setEnemyDescription("files/enemies/medium/gorewire_the_skeletal_lich.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.MEDIUM)
                        .name("Gorewire, the skeletal lich")
                        .currentHealth(4)
                        .health(4)
                        .description(enemyDescription)
                        .perception(14)
                        .rangedDamage(1)
                        .strength(11)
                        .meleeDamage(0)
                        .magic(15)
                        .magicDamage(3)
                        .revard(4)
                        .build();
                break;

            case 3:
                setEnemyDescription("files/enemies/medium/mysterious_predator.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.MEDIUM)
                        .name("Mysterious predator")
                        .currentHealth(3)
                        .health(3)
                        .description(enemyDescription)
                        .perception(13)
                        .rangedDamage(1)
                        .strength(16)
                        .meleeDamage(3)
                        .magic(11)
                        .magicDamage(0)
                        .revard(3)
                        .build();
                break;
        }

        return enemy;
    }

    public Enemy createHardEnemy(){

        Random generator = new Random();
        int enemyId = generator.nextInt(1) + 1;

        switch (enemyId){
            case 1:
                setEnemyDescription("files/enemies/hard/grosscaster_the_unstoppable.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.HARD)
                        .name("Grosscaster, the unstoppable")
                        .currentHealth(5)
                        .health(5)
                        .description(enemyDescription)
                        .perception(12)
                        .rangedDamage(2)
                        .strength(14)
                        .meleeDamage(2)
                        .magic(18)
                        .magicDamage(4)
                        .revard(7)
                        .build();
                break;

            case 2:
                setEnemyDescription("files/enemies/hard/wyvern_from_the_abandoned_temple.txt"); //tu sciezka do pliku

                enemy = new Enemy.Builder()
                        .rank(ERank.HARD)
                        .name("Wyvern from the abandoned temple")
                        .currentHealth(4)
                        .health(4)
                        .description(enemyDescription)
                        .perception(16)
                        .rangedDamage(2)
                        .strength(13)
                        .meleeDamage(4)
                        .magic(13)
                        .magicDamage(1)
                        .revard(5)
                        .build();
                break;
        }

        return enemy;
    }

    public Enemy createUltimateEnemy(){
        setEnemyDescription("files/enemies/ultimate/king_of_the_colosseum.txt");

        enemy = new Enemy.Builder()
                .rank(ERank.ULTIMATE)
                .name("King of the colosseum")
                .currentHealth(7)
                .health(7)
                .description(enemyDescription)
                .perception(20)
                .rangedDamage(4)
                .strength(18)
                .meleeDamage(4)
                .magic(16)
                .magicDamage(2)
                .revard(20)
                .build();

        return enemy;
    }
}
