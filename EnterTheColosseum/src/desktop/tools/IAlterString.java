package desktop.tools;

import com.googlecode.lanterna.terminal.Terminal;

/**
 * Created by Dom on 2016-12-10.
 */
public interface IAlterString {
    String text();
    void switchColor(Terminal.Color color);
    void print(int x, int y);
}
