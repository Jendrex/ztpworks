package desktop.tools;



import desktop.items.RealArmor;
import desktop.items.RealWeapon;

import java.util.ArrayList;

/**
 * Created by jedrek506 on 06.11.16.
 */
public class ItemFactory {
    //private Item item;
    private RealArmor armor;
    private RealWeapon weapon;
    public ArrayList<RealArmor> ArmorList;
    public ArrayList<RealWeapon> WeaponList;

    public ItemFactory()
    {
        CreateArmorList();
        CreateWeaponList();
    }


    public void CreateArmorList()
    {
        ArmorList = new ArrayList<RealArmor>();
        armor = new RealArmor.Builder()
                .name("Leather Armor")
                .cost(3)
                .hpMod(1)
                .description("Basic armor good for starting warrior")
                .build();
        ArmorList.add(armor);

        armor = new RealArmor.Builder()
                .name("Steel Armor")
                .cost(5)
                .hpMod(2)
                .description("Solid all-around armor, providing solid defense")
                .build();
        ArmorList.add(armor);

        armor = new RealArmor.Builder()
                .name("Gold-hemmed black robes")
                .cost(6)
                .hpMod(3)
                .description("Light and surprisingly well protecting armor, imbued with magic for defence")
                .build();
        ArmorList.add(armor);

        armor = new RealArmor.Builder()
                .name("Plate Armor")
                .cost(10)
                .hpMod(4)
                .description("Heavy armor covering entire body with reinforced steel")
                .build();
        ArmorList.add(armor);

        armor = new RealArmor.Builder()
                .name("Emperor Battle Armor")
                .cost(20)
                .hpMod(6)
                .description("Covered in gold and silver, this armor provides protection worthy of kings")
                .build();
        ArmorList.add(armor);
    }

    public void CreateWeaponList()
    {
        WeaponList = new ArrayList<RealWeapon>();
        weapon = new RealWeapon.Builder()
                .name("Dagger")
                .cost(1)
                .strengthMod(2)
                .meleeDamageMod(0)
                .perceptionMod(0)
                .rangedDamageMod(0)
                .magicMod(0)
                .magicDamageMod(0)
                .description("Extremely basic weapon that deals pitiful damage")
                .build();
        WeaponList.add(weapon);

        weapon = new RealWeapon.Builder()
                .name("Shortsword")
                .cost(3)
                .strengthMod(2)
                .meleeDamageMod(1)
                .perceptionMod(1)
                .rangedDamageMod(0)
                .magicMod(1)
                .magicDamageMod(0)
                .description("Standard basic soldier weapon, provides decent melee damage")
                .build();
        WeaponList.add(weapon);

        weapon = new RealWeapon.Builder()
                .name("Battle Axe")
                .cost(5)
                .strengthMod(3)
                .meleeDamageMod(2)
                .perceptionMod(1)
                .rangedDamageMod(0)
                .magicMod(0)
                .magicDamageMod(0)
                .description("Good weapon for brute force melee warriors, dealing good melee damage")
                .build();
        WeaponList.add(weapon);

        weapon = new RealWeapon.Builder()
                .name("Longbow")
                .cost(6)
                .strengthMod(2)
                .meleeDamageMod(0)
                .perceptionMod(4)
                .rangedDamageMod(2)
                .magicMod(1)
                .magicDamageMod(0)
                .description("Standard ranged weapon with good accuracy")
                .build();
        WeaponList.add(weapon);

        weapon = new RealWeapon.Builder()
                .name("Staff")
                .cost(6)
                .strengthMod(1)
                .meleeDamageMod(0)
                .perceptionMod(0)
                .rangedDamageMod(0)
                .magicMod(4)
                .magicDamageMod(2)
                .description("Common staff used by mages to support casting of their spells")
                .build();
        WeaponList.add(weapon);

        weapon = new RealWeapon.Builder()
                .name("Semi-automatic Crossbow")
                .cost(10)
                .strengthMod(3)
                .meleeDamageMod(1)
                .perceptionMod(5)
                .rangedDamageMod(2)
                .magicMod(2)
                .magicDamageMod(0)
                .description("High-tech crossbow having great accuracy")
                .build();
        WeaponList.add(weapon);

        weapon = new RealWeapon.Builder()
                .name("Lightning rod")
                .cost(7)
                .strengthMod(2)
                .meleeDamageMod(0)
                .perceptionMod(2)
                .rangedDamageMod(0)
                .magicMod(5)
                .magicDamageMod(2)
                .description("Staff imbued with lightning, gives great tracking to spells cast")
                .build();
        WeaponList.add(weapon);

        weapon = new RealWeapon.Builder()
                .name("Flaming Sword of Doom")
                .cost(20)
                .strengthMod(6)
                .meleeDamageMod(4)
                .perceptionMod(1)
                .rangedDamageMod(0)
                .magicMod(6)
                .magicDamageMod(1)
                .description("Sword powered by the power of flame - does great in melee and magic")
                .build();
        WeaponList.add(weapon);

    }

}
